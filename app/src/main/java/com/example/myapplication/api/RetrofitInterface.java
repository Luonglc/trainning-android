package com.example.myapplication.api;

import com.example.myapplication.model.APIModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterface {
    @GET("photos/?client_id=b20edcf373a47beb2fb48e299e59fb3fa8a6504e10ef4d4242cf2bd5b7c5c167")
    Call<List<APIModel>> getImage();
    @GET("photos/?client_id=b20edcf373a47beb2fb48e299e59fb3fa8a6504e10ef4d4242cf2bd5b7c5c167")
    Call<List<APIModel>> getImagePage(@Query("page") int page);
}
