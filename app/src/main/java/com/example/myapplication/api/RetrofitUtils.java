package com.example.myapplication.api;

public class RetrofitUtils {
    public static final String BASE_URL = "https://api.unsplash.com/";
    public static RetrofitInterface getService(){
        return RetrofitClient.getClRetrofit(BASE_URL).create(RetrofitInterface.class);
    }
}
