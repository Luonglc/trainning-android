package com.example.myapplication.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.SongModel;

import java.util.List;

public class ListSongAdapter extends RecyclerView.Adapter<ListSongAdapter.ViewHolder> {
    private List<SongModel> listsong;

    public ListSongAdapter(List<SongModel> listsong) {
        this.listsong = listsong;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        SongModel song = listsong.get(i);
        viewHolder.txtname.setText(song.getTitle());
        viewHolder.txtartist.setText(song.getArtist());
        viewHolder.txtalbum.setText(song.getAlbum());
    }

    @Override
    public int getItemCount() {
        return listsong.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtname,txtartist,txtalbum;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtname = (TextView)itemView.findViewById(R.id.namesong);
            txtartist = (TextView)itemView.findViewById(R.id.artist);
            txtalbum = (TextView)itemView.findViewById(R.id.album);

        }
    }
}
