package com.example.myapplication.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.myapplication.Interface.ILoadMore;
import com.example.myapplication.R;
import com.example.myapplication.model.Model;

import java.util.ArrayList;
import java.util.List;

public class ModelAdapter extends RecyclerView.Adapter<ModelAdapter.MyViewHolder> {

    private ArrayList<Model> list;

    public ModelAdapter(ArrayList<Model> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.model_item,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Model model = list.get(i);
        myViewHolder.txtItem.setText(model.getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtItem;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItem = (TextView)itemView.findViewById(R.id.txtItem);
        }
    }
}

//class LoadingViewHolder extends RecyclerView.ViewHolder{
//    public ProgressBar progressBar;
//    public LoadingViewHolder(@NonNull View itemView) {
//        super(itemView);
//        progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);
//    }
//}
//
//class ItemViewHolder extends RecyclerView.ViewHolder{
//    public TextView txtItem;
//    public ItemViewHolder(@NonNull View itemView) {
//        super(itemView);
//        txtItem = (TextView)itemView.findViewById(R.id.txtItem);
//    }
//}
//
//public class ModelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
//    private final int VIEW_TYPE_ITEM = 0, VIEW_TYPE_LOADING = 1;
//    ILoadMore loadMore;
//    boolean isLoading;
//    Activity activity;
//    ArrayList<Model> items;
//    int lastVisible,totalItemCount;
//    int visibleIThreshold = 12 ;
//
//    public ModelAdapter(RecyclerView recyclerView,Activity activity, ArrayList<Model> items) {
//        this.activity = activity;
//        this.items = items;
//
//        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                totalItemCount = linearLayoutManager.getItemCount();
//                lastVisible = linearLayoutManager.findLastVisibleItemPosition();
//                if(!isLoading && totalItemCount<= (lastVisible+ visibleIThreshold)){
//                    if(loadMore != null){
//                        loadMore.onLoadMode();
//                    }
//                    isLoading=true;
//                }
//            }
//        });
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return items.get(position) == null ? VIEW_TYPE_LOADING:VIEW_TYPE_ITEM;
//    }
//
//    public void setLoadMore(ILoadMore loadMore){
//        this.loadMore = loadMore;
//    }
//
//    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        if(i == VIEW_TYPE_ITEM){
//            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.model_item,viewGroup,false);
//            return new ItemViewHolder(view);
//        }else if(i == VIEW_TYPE_LOADING){
//            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_loading,viewGroup,false);
//            return new LoadingViewHolder(view);
//        }
//        return null;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
//        if(viewHolder instanceof ItemViewHolder){
//            Model model = items.get(i);
//            ItemViewHolder holder = (ItemViewHolder) viewHolder;
//            holder.txtItem.setText(model.getTitle());
//        }else if(viewHolder instanceof LoadingViewHolder){
//            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)viewHolder;
//            loadingViewHolder.progressBar.setIndeterminate(true);
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return items.size();
//    }
//
//    public void setLoaded(){
//        isLoading = false;
//    }
//}