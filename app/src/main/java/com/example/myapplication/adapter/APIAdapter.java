package com.example.myapplication.adapter;

import android.app.Activity;
import android.media.Image;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.example.myapplication.Interface.ILoadMore;
import com.example.myapplication.R;
import com.example.myapplication.model.APIModel;

import java.util.List;

class LoadingViewHolder extends RecyclerView.ViewHolder{
    public ProgressBar pr;
    public LoadingViewHolder(@NonNull View itemView) {
        super(itemView);
        pr = (ProgressBar)itemView.findViewById(R.id.progressBar);
    }
}

class ImageViewHolder extends RecyclerView.ViewHolder{
    public ImageView img;
    public ImageViewHolder(@NonNull View itemView) {
        super(itemView);
        img = (ImageView)itemView.findViewById(R.id.item_image);
    }
}
public class APIAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private ILoadMore loadMore;
    private boolean isLoading ;
    private Activity activity;
    private List<APIModel> items;
     int visibleThreshold = 3;
     int lastVisibleItem, totalItemCound;

    public APIAdapter(RecyclerView recyclerView,Activity activity, List<APIModel> items) {
        this.activity = activity;
        this.items = items;

        final LinearLayoutManager linearLayout = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCound = linearLayout.getItemCount();
                lastVisibleItem = linearLayout.findLastVisibleItemPosition();
                if(!isLoading && totalItemCound <= (lastVisibleItem+visibleThreshold)){
                    if(loadMore != null){
                        loadMore.onLoadMode();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position)==null?VIEW_TYPE_LOADING:VIEW_TYPE_ITEM;
    }

    public void setLoadMore(ILoadMore loadMore){
        this.loadMore = loadMore;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(i == VIEW_TYPE_ITEM){
            View view = LayoutInflater.from(activity).inflate(R.layout.api_item,viewGroup,false);
            return new ImageViewHolder(view);
        }else if(i == VIEW_TYPE_LOADING ){
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading,viewGroup,false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(viewHolder instanceof ImageViewHolder){
            APIModel item = items.get(i);
            ImageViewHolder holder = (ImageViewHolder) viewHolder;
            Glide.with(activity.getApplicationContext()).load(item.getUrls().getFull()).placeholder(R.drawable.placeholder
            ).into(holder.img);
        }else if(viewHolder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder)viewHolder;
            loadingViewHolder.pr.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setLoaded(){
        isLoading = false;
    }
}
