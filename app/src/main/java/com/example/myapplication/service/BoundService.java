package com.example.myapplication.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class BoundService extends Service {
    private final IBinder mBinder = new MyBinder();
    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("TAG","onCreate");
    }
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("TAG","onBind");
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e("TAG","onUnbind");
        return super.onUnbind(intent);
    }


    @Override
    public void onRebind(Intent intent) {
        Log.e("TAG","onRebind");
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("TAG","onDestroy");
    }

    public class MyBinder extends Binder {
        public BoundService getService(){
            return BoundService.this;
        }
    }
}
