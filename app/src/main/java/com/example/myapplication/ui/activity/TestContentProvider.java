package com.example.myapplication.ui.activity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.adapter.ListSongAdapter;
import com.example.myapplication.model.SongModel;

import java.util.ArrayList;

public class TestContentProvider extends AppCompatActivity {

    public static final int REQUEST_CODE = 123;
    ArrayList<SongModel> listSong = new ArrayList<>();
    private RecyclerView recyclerView;
    private ListSongAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_content_provider_activity);
        checkPermission();
        Log.e("TAG","Size:" +listSong.size());
        recyclerView = (RecyclerView)findViewById(R.id.recyleview);
        adapter = new ListSongAdapter(listSong);
        RecyclerView.LayoutManager layoutManager =  new LinearLayoutManager(getApplicationContext());
        ((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    //Kiểm tra quyền đọc file external
    private void checkPermission() {
        if(Build.VERSION.SDK_INT >= 23){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED
            ){
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WAKE_LOCK},
                        REQUEST_CODE
                );
            }
        }
    }

    //Xử lý kết quả yêu cầu cấp quyền
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CODE){
            if(grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED
            ){
                loadListSong();
            }
        }
    }

    // Load bài hát từ sdcard
    private void loadListSong() {
        ContentResolver contentResolver = getContentResolver();
        Uri  uri= android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(uri , null, null, null, null);
        if( cursor != null && cursor.moveToFirst() ){
            //get column
            int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int idColumn = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int artistColmn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int albumColmn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);

            // add to list
            do{
                long id = cursor.getLong(idColumn);
                String title = cursor.getString(titleColumn);
                String artist = cursor.getString(artistColmn);
                String album = cursor.getString(albumColmn);
                listSong.add(new SongModel(id,title,artist,album));
            }while(cursor.moveToNext());
        }
    }


}
