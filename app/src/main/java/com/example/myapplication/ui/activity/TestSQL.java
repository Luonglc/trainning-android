package com.example.myapplication.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Database.DatabaseHelper;
import com.example.myapplication.R;
import com.example.myapplication.adapter.ModelAdapter;
import com.example.myapplication.model.Model;

import java.util.ArrayList;
import java.util.List;

public class TestSQL extends AppCompatActivity {

    private ArrayList<Model> listmodel ;
    private RecyclerView recyclerView;
    private ModelAdapter adapter;

    Button btnAdd;
    EditText editTitle;

    private DatabaseHelper db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_sqlite_activity);

        initViews();

        db = new DatabaseHelper(this);



        listToRecycleView();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });



    }

    private void initViews() {
        btnAdd = (Button)findViewById(R.id.btnadd);
        editTitle = (EditText) findViewById(R.id.edittitle);
        recyclerView = (RecyclerView)findViewById(R.id.recyleviewModel);
    }

    //Add item
    private void addItem(){
        String title = editTitle.getText().toString();

        if(title.equals("")){
            Toast.makeText(this, "Chưa nhập title", Toast.LENGTH_SHORT).show();
        }else{
            Model model = new Model();
            model.setTitle(title);
            db.addDB(model);
            listmodel.add(model);
            adapter.notifyDataSetChanged();
            Toast.makeText(this, "Add success", Toast.LENGTH_SHORT).show();
        }
    }

    // list item => recycleview
    private void listToRecycleView(){
        listmodel = db.getAll();
        adapter = new ModelAdapter(listmodel);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        //((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }


}
