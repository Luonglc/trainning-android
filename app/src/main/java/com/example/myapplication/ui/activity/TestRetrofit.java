package com.example.myapplication.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.myapplication.Interface.ILoadMore;
import com.example.myapplication.R;
import com.example.myapplication.adapter.APIAdapter;
import com.example.myapplication.api.RetrofitInterface;
import com.example.myapplication.api.RetrofitUtils;
import com.example.myapplication.model.APIModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestRetrofit extends AppCompatActivity {

    private RetrofitInterface mService;
    private RecyclerView recyclerView;
    private List<APIModel> list;
    private APIAdapter adapter;
    private int numberPage = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retrofit_activity);

        mService = RetrofitUtils.getService();

        setUpRecycleView();
        getFirstPage();
    }

    private void setUpRecycleView(){
        recyclerView = findViewById(R.id.api_recycleview);
        list = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new APIAdapter(recyclerView,this,list);
        recyclerView.setAdapter(adapter);
        setLoadMoreEvent();
    }

    private void setLoadMoreEvent() {
        numberPage++;
        adapter.setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMode() {
                list.add(null);
                adapter.notifyItemInserted(list.size()-1);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        list.remove(list.size()-1);
                        adapter.notifyItemRemoved(list.size());

                        //add new data

                        getImageForPage(numberPage);

                    }
                },3000);
            }
        });
    }

    private void getFirstPage(){
        mService.getImage().enqueue(new Callback<List<APIModel>>() {
            @Override
            public void onResponse(Call<List<APIModel>> call, Response<List<APIModel>> response) {
                List<APIModel> items = response.body();
                Log.e("TAG",list.size()+"");
                if(items.size() == 0 ){
                    return;
                }else{
                    list.addAll(items);
                    Log.e("TAG",list.size()+"");
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<APIModel>> call, Throwable t) {
                Log.e("TAG",t+"");
            }
        });
    }
    private void getImageForPage(int page){
        mService.getImagePage(page).enqueue(new Callback<List<APIModel>>() {
            @Override
            public void onResponse(Call<List<APIModel>> call, Response<List<APIModel>> response) {
                List<APIModel> items = response.body();
                Log.e("TAG",list.size()+"");
                if(items.size() == 0 ){
                    return;
                }else{
                    list.addAll(items);
                    Log.e("TAG",list.size()+"");
                    adapter.notifyDataSetChanged();
                    adapter.setLoaded();
                }
            }

            @Override
            public void onFailure(Call<List<APIModel>> call, Throwable t) {

            }
        });
    }

}