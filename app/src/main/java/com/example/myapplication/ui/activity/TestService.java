package com.example.myapplication.ui.activity;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.R;
import com.example.myapplication.service.BoundService;
import com.example.myapplication.service.MyService;

public class TestService extends AppCompatActivity implements View.OnClickListener {
    Button btnStart,btnStop,btnBind,btnUnBind;
    BoundService boundService;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_service_activity);

        btnStart = (Button)findViewById(R.id.btn_start_service);
        btnStop = (Button)findViewById(R.id.btn_stop_service);
        btnBind = (Button)findViewById(R.id.btnBind);
        btnUnBind = (Button)findViewById(R.id.btnUnBind);
        btnStart.setOnClickListener(this);
        btnStop.setOnClickListener(this);
        btnBind.setOnClickListener(this);
        btnUnBind.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, MyService.class);
        Intent boundIntent = new Intent(this,BoundService.class);
        switch (v.getId()){
            case R.id.btn_start_service:
                startService(intent);
                break;
            case R.id.btn_stop_service:
                stopService(intent);
                break;
            case R.id.btnBind:
                bindService(boundIntent,serviceConnection, Context.BIND_AUTO_CREATE);
                break;
            case R.id.btnUnBind:
                unbindService(serviceConnection);

        }
    }

    public ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinder) {
            BoundService.MyBinder binder = (BoundService.MyBinder) iBinder ;
            boundService = binder.getService();
            Log.e("TAG","Connect");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
}
