package com.example.myapplication.ui.activity;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.BroadCastReciever.ChangeNetWork;

public class TestBroadCastReciever extends AppCompatActivity {
    BroadcastReceiver reciever;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_broatcast_activity);

         reciever = new ChangeNetWork();
        final IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(reciever,intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(reciever);
    }
}
