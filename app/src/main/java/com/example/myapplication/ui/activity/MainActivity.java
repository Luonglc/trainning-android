package com.example.myapplication.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnCP,btnBr,btnService,btnSQL,btnAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initOnClickListener();
    }

    private void initOnClickListener() {
        btnBr.setOnClickListener(this);
        btnCP.setOnClickListener(this);
        btnService.setOnClickListener(this);
        btnSQL.setOnClickListener(this);
        btnAPI.setOnClickListener(this);
    }

    private void initView() {
        btnCP = (Button)findViewById(R.id.btnCP);
        btnBr = (Button)findViewById(R.id.btnBr);
        btnService = (Button)findViewById(R.id.btnService);
        btnSQL = (Button)findViewById(R.id.btnSql);
        btnAPI = (Button)findViewById(R.id.btnAPI);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnCP:
                Intent intent = new Intent(MainActivity.this,TestContentProvider.class);
                startActivity(intent);
                break;
            case R.id.btnBr:
                Intent intent1 = new Intent(MainActivity.this,TestBroadCastReciever.class);
                startActivity(intent1);
                break;
            case R.id.btnService:
                Intent intent2 = new Intent(MainActivity.this,TestService.class);
                startActivity(intent2);
                break;
            case R.id.btnSql:
                Intent intent3 = new Intent(MainActivity.this,TestSQL.class);
                startActivity(intent3);
                break;
            case R.id.btnAPI:
                Intent intent4 = new Intent(MainActivity.this,TestRetrofit.class);
                startActivity(intent4);
                break;
        }
    }
}
