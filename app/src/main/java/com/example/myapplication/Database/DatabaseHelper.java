package com.example.myapplication.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.myapplication.model.Model;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "SQLite";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "NAMEDB";
    private static final String TABLE = "NAMETB";
    private static final String COLUMN_ID = "cl_id";
    private static final String COLUMN_TITLE = "cl_title";
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Tạo bảng
    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryCreateTB =  "CREATE TABLE "+TABLE+" ("
                +COLUMN_ID+" INTEGER PRIMARY KEY,"+COLUMN_TITLE+" TEXT)";
        db.execSQL(queryCreateTB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Xoá bảng nếu tồn tại
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE);
        onCreate(db);
    }

    // Thêm phần tử vào db
    public void addDB(Model model){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE,model.getTitle());
        db.insert(TABLE,null,values);
        db.close();
    }

    //Lấy tất cả phần tử
    public ArrayList<Model> getAll(){
        ArrayList<Model> list = new ArrayList<>();
        String queryGetAll = "SELECT * FROM "+TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(queryGetAll,null);

        if(cursor.moveToFirst()){
            do{
                Model model = new Model();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setTitle(cursor.getString(1));
                list.add(model);
            }while(cursor.moveToNext());
        }

        return list;
    }


}
